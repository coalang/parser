// example1.go
package parser

import (
	"fmt"
	"github.com/antlr/antlr4/runtime/Go/antlr"
)

func main() {
	// Setup the input
	is := antlr.NewInputStream("1.2 + 2.0 ** 3")

	// Create the Lexer
	lexer := old.NewCoaLexer(is)

	// Read all tokens
	for {
		t := lexer.NextToken()
		if t.GetTokenType() == antlr.TokenEOF {
			break
		}
		fmt.Printf("%s (%q)\n", lexer.SymbolicNames[t.GetTokenType()], t.GetText())
	}
}
