// Code generated from Coa.g4 by ANTLR 4.8. DO NOT EDIT.

package parser // Coa

import (
	"fmt"
	"reflect"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = reflect.Copy
var _ = strconv.Itoa

var parserATN = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 3, 14, 170,
	4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7, 9, 7,
	4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12, 4, 13,
	9, 13, 4, 14, 9, 14, 4, 15, 9, 15, 4, 16, 9, 16, 4, 17, 9, 17, 4, 18, 9,
	18, 4, 19, 9, 19, 4, 20, 9, 20, 4, 21, 9, 21, 4, 22, 9, 22, 3, 2, 3, 2,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 5, 4, 52, 10, 4, 3, 5, 7, 5, 55, 10, 5, 12,
	5, 14, 5, 58, 11, 5, 3, 5, 3, 5, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3,
	6, 5, 6, 69, 10, 6, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3,
	7, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3,
	7, 5, 7, 92, 10, 7, 3, 8, 7, 8, 95, 10, 8, 12, 8, 14, 8, 98, 11, 8, 3,
	8, 3, 8, 3, 9, 3, 9, 3, 9, 3, 10, 3, 10, 3, 11, 3, 11, 5, 11, 109, 10,
	11, 3, 12, 3, 12, 3, 12, 3, 12, 3, 13, 3, 13, 3, 13, 3, 14, 3, 14, 5, 14,
	120, 10, 14, 7, 14, 122, 10, 14, 12, 14, 14, 14, 125, 11, 14, 3, 15, 3,
	15, 5, 15, 129, 10, 15, 7, 15, 131, 10, 15, 12, 15, 14, 15, 134, 11, 15,
	3, 16, 3, 16, 3, 16, 3, 16, 3, 17, 3, 17, 3, 17, 3, 18, 3, 18, 3, 18, 3,
	19, 3, 19, 5, 19, 148, 10, 19, 3, 19, 3, 19, 3, 20, 7, 20, 153, 10, 20,
	12, 20, 14, 20, 156, 11, 20, 3, 20, 3, 20, 3, 21, 3, 21, 3, 21, 3, 22,
	3, 22, 7, 22, 165, 10, 22, 12, 22, 14, 22, 168, 11, 22, 3, 22, 3, 166,
	2, 23, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34,
	36, 38, 40, 42, 2, 2, 2, 171, 2, 44, 3, 2, 2, 2, 4, 46, 3, 2, 2, 2, 6,
	51, 3, 2, 2, 2, 8, 56, 3, 2, 2, 2, 10, 68, 3, 2, 2, 2, 12, 91, 3, 2, 2,
	2, 14, 96, 3, 2, 2, 2, 16, 101, 3, 2, 2, 2, 18, 104, 3, 2, 2, 2, 20, 108,
	3, 2, 2, 2, 22, 110, 3, 2, 2, 2, 24, 114, 3, 2, 2, 2, 26, 123, 3, 2, 2,
	2, 28, 132, 3, 2, 2, 2, 30, 135, 3, 2, 2, 2, 32, 139, 3, 2, 2, 2, 34, 142,
	3, 2, 2, 2, 36, 145, 3, 2, 2, 2, 38, 154, 3, 2, 2, 2, 40, 159, 3, 2, 2,
	2, 42, 162, 3, 2, 2, 2, 44, 45, 5, 6, 4, 2, 45, 3, 3, 2, 2, 2, 46, 47,
	7, 3, 2, 2, 47, 48, 5, 6, 4, 2, 48, 49, 7, 4, 2, 2, 49, 5, 3, 2, 2, 2,
	50, 52, 5, 8, 5, 2, 51, 50, 3, 2, 2, 2, 51, 52, 3, 2, 2, 2, 52, 7, 3, 2,
	2, 2, 53, 55, 5, 12, 7, 2, 54, 53, 3, 2, 2, 2, 55, 58, 3, 2, 2, 2, 56,
	54, 3, 2, 2, 2, 56, 57, 3, 2, 2, 2, 57, 59, 3, 2, 2, 2, 58, 56, 3, 2, 2,
	2, 59, 60, 5, 10, 6, 2, 60, 9, 3, 2, 2, 2, 61, 69, 5, 14, 8, 2, 62, 69,
	5, 20, 11, 2, 63, 69, 5, 30, 16, 2, 64, 69, 5, 32, 17, 2, 65, 69, 5, 34,
	18, 2, 66, 69, 5, 36, 19, 2, 67, 69, 5, 42, 22, 2, 68, 61, 3, 2, 2, 2,
	68, 62, 3, 2, 2, 2, 68, 63, 3, 2, 2, 2, 68, 64, 3, 2, 2, 2, 68, 65, 3,
	2, 2, 2, 68, 66, 3, 2, 2, 2, 68, 67, 3, 2, 2, 2, 69, 11, 3, 2, 2, 2, 70,
	71, 5, 14, 8, 2, 71, 72, 7, 14, 2, 2, 72, 92, 3, 2, 2, 2, 73, 74, 5, 20,
	11, 2, 74, 75, 7, 14, 2, 2, 75, 92, 3, 2, 2, 2, 76, 77, 5, 30, 16, 2, 77,
	78, 7, 14, 2, 2, 78, 92, 3, 2, 2, 2, 79, 80, 5, 32, 17, 2, 80, 81, 7, 14,
	2, 2, 81, 92, 3, 2, 2, 2, 82, 83, 5, 34, 18, 2, 83, 84, 7, 14, 2, 2, 84,
	92, 3, 2, 2, 2, 85, 86, 5, 36, 19, 2, 86, 87, 7, 14, 2, 2, 87, 92, 3, 2,
	2, 2, 88, 89, 5, 42, 22, 2, 89, 90, 7, 13, 2, 2, 90, 92, 3, 2, 2, 2, 91,
	70, 3, 2, 2, 2, 91, 73, 3, 2, 2, 2, 91, 76, 3, 2, 2, 2, 91, 79, 3, 2, 2,
	2, 91, 82, 3, 2, 2, 2, 91, 85, 3, 2, 2, 2, 91, 88, 3, 2, 2, 2, 92, 13,
	3, 2, 2, 2, 93, 95, 5, 16, 9, 2, 94, 93, 3, 2, 2, 2, 95, 98, 3, 2, 2, 2,
	96, 94, 3, 2, 2, 2, 96, 97, 3, 2, 2, 2, 97, 99, 3, 2, 2, 2, 98, 96, 3,
	2, 2, 2, 99, 100, 5, 18, 10, 2, 100, 15, 3, 2, 2, 2, 101, 102, 5, 28, 15,
	2, 102, 103, 7, 9, 2, 2, 103, 17, 3, 2, 2, 2, 104, 105, 5, 28, 15, 2, 105,
	19, 3, 2, 2, 2, 106, 109, 5, 24, 13, 2, 107, 109, 5, 22, 12, 2, 108, 106,
	3, 2, 2, 2, 108, 107, 3, 2, 2, 2, 109, 21, 3, 2, 2, 2, 110, 111, 7, 7,
	2, 2, 111, 112, 5, 26, 14, 2, 112, 113, 7, 7, 2, 2, 113, 23, 3, 2, 2, 2,
	114, 115, 7, 7, 2, 2, 115, 116, 7, 7, 2, 2, 116, 25, 3, 2, 2, 2, 117, 119,
	7, 11, 2, 2, 118, 120, 5, 30, 16, 2, 119, 118, 3, 2, 2, 2, 119, 120, 3,
	2, 2, 2, 120, 122, 3, 2, 2, 2, 121, 117, 3, 2, 2, 2, 122, 125, 3, 2, 2,
	2, 123, 121, 3, 2, 2, 2, 123, 124, 3, 2, 2, 2, 124, 27, 3, 2, 2, 2, 125,
	123, 3, 2, 2, 2, 126, 128, 7, 11, 2, 2, 127, 129, 5, 30, 16, 2, 128, 127,
	3, 2, 2, 2, 128, 129, 3, 2, 2, 2, 129, 131, 3, 2, 2, 2, 130, 126, 3, 2,
	2, 2, 131, 134, 3, 2, 2, 2, 132, 130, 3, 2, 2, 2, 132, 133, 3, 2, 2, 2,
	133, 29, 3, 2, 2, 2, 134, 132, 3, 2, 2, 2, 135, 136, 7, 8, 2, 2, 136, 137,
	5, 6, 4, 2, 137, 138, 7, 8, 2, 2, 138, 31, 3, 2, 2, 2, 139, 140, 5, 36,
	19, 2, 140, 141, 5, 4, 3, 2, 141, 33, 3, 2, 2, 2, 142, 143, 5, 14, 8, 2,
	143, 144, 5, 36, 19, 2, 144, 35, 3, 2, 2, 2, 145, 147, 7, 5, 2, 2, 146,
	148, 5, 38, 20, 2, 147, 146, 3, 2, 2, 2, 147, 148, 3, 2, 2, 2, 148, 149,
	3, 2, 2, 2, 149, 150, 7, 6, 2, 2, 150, 37, 3, 2, 2, 2, 151, 153, 5, 40,
	21, 2, 152, 151, 3, 2, 2, 2, 153, 156, 3, 2, 2, 2, 154, 152, 3, 2, 2, 2,
	154, 155, 3, 2, 2, 2, 155, 157, 3, 2, 2, 2, 156, 154, 3, 2, 2, 2, 157,
	158, 5, 10, 6, 2, 158, 39, 3, 2, 2, 2, 159, 160, 5, 10, 6, 2, 160, 161,
	7, 14, 2, 2, 161, 41, 3, 2, 2, 2, 162, 166, 7, 12, 2, 2, 163, 165, 11,
	2, 2, 2, 164, 163, 3, 2, 2, 2, 165, 168, 3, 2, 2, 2, 166, 167, 3, 2, 2,
	2, 166, 164, 3, 2, 2, 2, 167, 43, 3, 2, 2, 2, 168, 166, 3, 2, 2, 2, 15,
	51, 56, 68, 91, 96, 108, 119, 123, 128, 132, 147, 154, 166,
}
var deserializer = antlr.NewATNDeserializer(nil)
var deserializedATN = deserializer.DeserializeFromUInt16(parserATN)

var literalNames = []string{
	"", "'['", "']'", "'('", "')'", "'''", "'\"'", "'.'", "", "", "'#'",
}
var symbolicNames = []string{
	"", "OBRACK", "CBRACK", "OPAREN", "CPAREN", "SINGLEQUOTE", "DOUBLEQUOTE",
	"DOT", "ID", "IDINSIDE", "NUMBERSIGN", "NEWLINE", "SPACE",
}

var ruleNames = []string{
	"start", "block", "rawBlock", "rawBlockNonNil", "object", "objectDivider",
	"ref", "refInside", "refInsideNoDot", "text", "textNonNil", "textNil",
	"textFmted", "idFmted", "fmtText", "function", "call", "mapping", "mapNonNil",
	"objectSpace", "comment",
}
var decisionToDFA = make([]*antlr.DFA, len(deserializedATN.DecisionToState))

func init() {
	for index, ds := range deserializedATN.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

type CoaParser struct {
	*antlr.BaseParser
}

func NewCoaParser(input antlr.TokenStream) *CoaParser {
	this := new(CoaParser)

	this.BaseParser = antlr.NewBaseParser(input)

	this.Interpreter = antlr.NewParserATNSimulator(this, deserializedATN, decisionToDFA, antlr.NewPredictionContextCache())
	this.RuleNames = ruleNames
	this.LiteralNames = literalNames
	this.SymbolicNames = symbolicNames
	this.GrammarFileName = "Coa.g4"

	return this
}

// CoaParser tokens.
const (
	CoaParserEOF         = antlr.TokenEOF
	CoaParserOBRACK      = 1
	CoaParserCBRACK      = 2
	CoaParserOPAREN      = 3
	CoaParserCPAREN      = 4
	CoaParserSINGLEQUOTE = 5
	CoaParserDOUBLEQUOTE = 6
	CoaParserDOT         = 7
	CoaParserID          = 8
	CoaParserIDINSIDE    = 9
	CoaParserNUMBERSIGN  = 10
	CoaParserNEWLINE     = 11
	CoaParserSPACE       = 12
)

// CoaParser rules.
const (
	CoaParserRULE_start          = 0
	CoaParserRULE_block          = 1
	CoaParserRULE_rawBlock       = 2
	CoaParserRULE_rawBlockNonNil = 3
	CoaParserRULE_object         = 4
	CoaParserRULE_objectDivider  = 5
	CoaParserRULE_ref            = 6
	CoaParserRULE_refInside      = 7
	CoaParserRULE_refInsideNoDot = 8
	CoaParserRULE_text           = 9
	CoaParserRULE_textNonNil     = 10
	CoaParserRULE_textNil        = 11
	CoaParserRULE_textFmted      = 12
	CoaParserRULE_idFmted        = 13
	CoaParserRULE_fmtText        = 14
	CoaParserRULE_function       = 15
	CoaParserRULE_call           = 16
	CoaParserRULE_mapping        = 17
	CoaParserRULE_mapNonNil      = 18
	CoaParserRULE_objectSpace    = 19
	CoaParserRULE_comment        = 20
)

// IStartContext is an interface to support dynamic dispatch.
type IStartContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetRb returns the rb rule contexts.
	GetRb() IRawBlockContext

	// SetRb sets the rb rule contexts.
	SetRb(IRawBlockContext)

	// IsStartContext differentiates from other interfaces.
	IsStartContext()
}

type StartContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	rb     IRawBlockContext
}

func NewEmptyStartContext() *StartContext {
	var p = new(StartContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_start
	return p
}

func (*StartContext) IsStartContext() {}

func NewStartContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartContext {
	var p = new(StartContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_start

	return p
}

func (s *StartContext) GetParser() antlr.Parser { return s.parser }

func (s *StartContext) GetRb() IRawBlockContext { return s.rb }

func (s *StartContext) SetRb(v IRawBlockContext) { s.rb = v }

func (s *StartContext) RawBlock() IRawBlockContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRawBlockContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IRawBlockContext)
}

func (s *StartContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterStart(s)
	}
}

func (s *StartContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitStart(s)
	}
}

func (p *CoaParser) Start() (localctx IStartContext) {
	localctx = NewStartContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, CoaParserRULE_start)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(42)

		var _x = p.RawBlock()

		localctx.(*StartContext).rb = _x
	}

	return localctx
}

// IBlockContext is an interface to support dynamic dispatch.
type IBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetRb returns the rb rule contexts.
	GetRb() IRawBlockContext

	// SetRb sets the rb rule contexts.
	SetRb(IRawBlockContext)

	// IsBlockContext differentiates from other interfaces.
	IsBlockContext()
}

type BlockContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	rb     IRawBlockContext
}

func NewEmptyBlockContext() *BlockContext {
	var p = new(BlockContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_block
	return p
}

func (*BlockContext) IsBlockContext() {}

func NewBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BlockContext {
	var p = new(BlockContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_block

	return p
}

func (s *BlockContext) GetParser() antlr.Parser { return s.parser }

func (s *BlockContext) GetRb() IRawBlockContext { return s.rb }

func (s *BlockContext) SetRb(v IRawBlockContext) { s.rb = v }

func (s *BlockContext) OBRACK() antlr.TerminalNode {
	return s.GetToken(CoaParserOBRACK, 0)
}

func (s *BlockContext) CBRACK() antlr.TerminalNode {
	return s.GetToken(CoaParserCBRACK, 0)
}

func (s *BlockContext) RawBlock() IRawBlockContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRawBlockContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IRawBlockContext)
}

func (s *BlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BlockContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterBlock(s)
	}
}

func (s *BlockContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitBlock(s)
	}
}

func (p *CoaParser) Block() (localctx IBlockContext) {
	localctx = NewBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, CoaParserRULE_block)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(44)
		p.Match(CoaParserOBRACK)
	}
	{
		p.SetState(45)

		var _x = p.RawBlock()

		localctx.(*BlockContext).rb = _x
	}
	{
		p.SetState(46)
		p.Match(CoaParserCBRACK)
	}

	return localctx
}

// IRawBlockContext is an interface to support dynamic dispatch.
type IRawBlockContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetRbnnil returns the rbnnil rule contexts.
	GetRbnnil() IRawBlockNonNilContext

	// SetRbnnil sets the rbnnil rule contexts.
	SetRbnnil(IRawBlockNonNilContext)

	// IsRawBlockContext differentiates from other interfaces.
	IsRawBlockContext()
}

type RawBlockContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	rbnnil IRawBlockNonNilContext
}

func NewEmptyRawBlockContext() *RawBlockContext {
	var p = new(RawBlockContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_rawBlock
	return p
}

func (*RawBlockContext) IsRawBlockContext() {}

func NewRawBlockContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RawBlockContext {
	var p = new(RawBlockContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_rawBlock

	return p
}

func (s *RawBlockContext) GetParser() antlr.Parser { return s.parser }

func (s *RawBlockContext) GetRbnnil() IRawBlockNonNilContext { return s.rbnnil }

func (s *RawBlockContext) SetRbnnil(v IRawBlockNonNilContext) { s.rbnnil = v }

func (s *RawBlockContext) RawBlockNonNil() IRawBlockNonNilContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRawBlockNonNilContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IRawBlockNonNilContext)
}

func (s *RawBlockContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RawBlockContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RawBlockContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterRawBlock(s)
	}
}

func (s *RawBlockContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitRawBlock(s)
	}
}

func (p *CoaParser) RawBlock() (localctx IRawBlockContext) {
	localctx = NewRawBlockContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, CoaParserRULE_rawBlock)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(49)
	p.GetErrorHandler().Sync(p)

	if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 0, p.GetParserRuleContext()) == 1 {
		{
			p.SetState(48)

			var _x = p.RawBlockNonNil()

			localctx.(*RawBlockContext).rbnnil = _x
		}

	}

	return localctx
}

// IRawBlockNonNilContext is an interface to support dynamic dispatch.
type IRawBlockNonNilContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetObjdiv returns the objdiv rule contexts.
	GetObjdiv() IObjectDividerContext

	// GetObj returns the obj rule contexts.
	GetObj() IObjectContext

	// SetObjdiv sets the objdiv rule contexts.
	SetObjdiv(IObjectDividerContext)

	// SetObj sets the obj rule contexts.
	SetObj(IObjectContext)

	// IsRawBlockNonNilContext differentiates from other interfaces.
	IsRawBlockNonNilContext()
}

type RawBlockNonNilContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	objdiv IObjectDividerContext
	obj    IObjectContext
}

func NewEmptyRawBlockNonNilContext() *RawBlockNonNilContext {
	var p = new(RawBlockNonNilContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_rawBlockNonNil
	return p
}

func (*RawBlockNonNilContext) IsRawBlockNonNilContext() {}

func NewRawBlockNonNilContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RawBlockNonNilContext {
	var p = new(RawBlockNonNilContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_rawBlockNonNil

	return p
}

func (s *RawBlockNonNilContext) GetParser() antlr.Parser { return s.parser }

func (s *RawBlockNonNilContext) GetObjdiv() IObjectDividerContext { return s.objdiv }

func (s *RawBlockNonNilContext) GetObj() IObjectContext { return s.obj }

func (s *RawBlockNonNilContext) SetObjdiv(v IObjectDividerContext) { s.objdiv = v }

func (s *RawBlockNonNilContext) SetObj(v IObjectContext) { s.obj = v }

func (s *RawBlockNonNilContext) Object() IObjectContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IObjectContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IObjectContext)
}

func (s *RawBlockNonNilContext) AllObjectDivider() []IObjectDividerContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IObjectDividerContext)(nil)).Elem())
	var tst = make([]IObjectDividerContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IObjectDividerContext)
		}
	}

	return tst
}

func (s *RawBlockNonNilContext) ObjectDivider(i int) IObjectDividerContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IObjectDividerContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IObjectDividerContext)
}

func (s *RawBlockNonNilContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RawBlockNonNilContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RawBlockNonNilContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterRawBlockNonNil(s)
	}
}

func (s *RawBlockNonNilContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitRawBlockNonNil(s)
	}
}

func (p *CoaParser) RawBlockNonNil() (localctx IRawBlockNonNilContext) {
	localctx = NewRawBlockNonNilContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, CoaParserRULE_rawBlockNonNil)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(54)
	p.GetErrorHandler().Sync(p)
	_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 1, p.GetParserRuleContext())

	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			{
				p.SetState(51)

				var _x = p.ObjectDivider()

				localctx.(*RawBlockNonNilContext).objdiv = _x
			}

		}
		p.SetState(56)
		p.GetErrorHandler().Sync(p)
		_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 1, p.GetParserRuleContext())
	}
	{
		p.SetState(57)

		var _x = p.Object()

		localctx.(*RawBlockNonNilContext).obj = _x
	}

	return localctx
}

// IObjectContext is an interface to support dynamic dispatch.
type IObjectContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetRf returns the rf rule contexts.
	GetRf() IRefContext

	// GetTxt returns the txt rule contexts.
	GetTxt() ITextContext

	// GetFmttxt returns the fmttxt rule contexts.
	GetFmttxt() IFmtTextContext

	// GetFun returns the fun rule contexts.
	GetFun() IFunctionContext

	// GetCll returns the cll rule contexts.
	GetCll() ICallContext

	// GetMp returns the mp rule contexts.
	GetMp() IMappingContext

	// GetCmt returns the cmt rule contexts.
	GetCmt() ICommentContext

	// SetRf sets the rf rule contexts.
	SetRf(IRefContext)

	// SetTxt sets the txt rule contexts.
	SetTxt(ITextContext)

	// SetFmttxt sets the fmttxt rule contexts.
	SetFmttxt(IFmtTextContext)

	// SetFun sets the fun rule contexts.
	SetFun(IFunctionContext)

	// SetCll sets the cll rule contexts.
	SetCll(ICallContext)

	// SetMp sets the mp rule contexts.
	SetMp(IMappingContext)

	// SetCmt sets the cmt rule contexts.
	SetCmt(ICommentContext)

	// IsObjectContext differentiates from other interfaces.
	IsObjectContext()
}

type ObjectContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	rf     IRefContext
	txt    ITextContext
	fmttxt IFmtTextContext
	fun    IFunctionContext
	cll    ICallContext
	mp     IMappingContext
	cmt    ICommentContext
}

func NewEmptyObjectContext() *ObjectContext {
	var p = new(ObjectContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_object
	return p
}

func (*ObjectContext) IsObjectContext() {}

func NewObjectContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ObjectContext {
	var p = new(ObjectContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_object

	return p
}

func (s *ObjectContext) GetParser() antlr.Parser { return s.parser }

func (s *ObjectContext) GetRf() IRefContext { return s.rf }

func (s *ObjectContext) GetTxt() ITextContext { return s.txt }

func (s *ObjectContext) GetFmttxt() IFmtTextContext { return s.fmttxt }

func (s *ObjectContext) GetFun() IFunctionContext { return s.fun }

func (s *ObjectContext) GetCll() ICallContext { return s.cll }

func (s *ObjectContext) GetMp() IMappingContext { return s.mp }

func (s *ObjectContext) GetCmt() ICommentContext { return s.cmt }

func (s *ObjectContext) SetRf(v IRefContext) { s.rf = v }

func (s *ObjectContext) SetTxt(v ITextContext) { s.txt = v }

func (s *ObjectContext) SetFmttxt(v IFmtTextContext) { s.fmttxt = v }

func (s *ObjectContext) SetFun(v IFunctionContext) { s.fun = v }

func (s *ObjectContext) SetCll(v ICallContext) { s.cll = v }

func (s *ObjectContext) SetMp(v IMappingContext) { s.mp = v }

func (s *ObjectContext) SetCmt(v ICommentContext) { s.cmt = v }

func (s *ObjectContext) Ref() IRefContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRefContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IRefContext)
}

func (s *ObjectContext) Text() ITextContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITextContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITextContext)
}

func (s *ObjectContext) FmtText() IFmtTextContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFmtTextContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFmtTextContext)
}

func (s *ObjectContext) Function() IFunctionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFunctionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFunctionContext)
}

func (s *ObjectContext) Call() ICallContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICallContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ICallContext)
}

func (s *ObjectContext) Mapping() IMappingContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMappingContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMappingContext)
}

func (s *ObjectContext) Comment() ICommentContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICommentContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ICommentContext)
}

func (s *ObjectContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ObjectContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ObjectContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterObject(s)
	}
}

func (s *ObjectContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitObject(s)
	}
}

func (p *CoaParser) Object() (localctx IObjectContext) {
	localctx = NewObjectContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, CoaParserRULE_object)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(66)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 2, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(59)

			var _x = p.Ref()

			localctx.(*ObjectContext).rf = _x
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(60)

			var _x = p.Text()

			localctx.(*ObjectContext).txt = _x
		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(61)

			var _x = p.FmtText()

			localctx.(*ObjectContext).fmttxt = _x
		}

	case 4:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(62)

			var _x = p.Function()

			localctx.(*ObjectContext).fun = _x
		}

	case 5:
		p.EnterOuterAlt(localctx, 5)
		{
			p.SetState(63)

			var _x = p.Call()

			localctx.(*ObjectContext).cll = _x
		}

	case 6:
		p.EnterOuterAlt(localctx, 6)
		{
			p.SetState(64)

			var _x = p.Mapping()

			localctx.(*ObjectContext).mp = _x
		}

	case 7:
		p.EnterOuterAlt(localctx, 7)
		{
			p.SetState(65)

			var _x = p.Comment()

			localctx.(*ObjectContext).cmt = _x
		}

	}

	return localctx
}

// IObjectDividerContext is an interface to support dynamic dispatch.
type IObjectDividerContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetRf returns the rf rule contexts.
	GetRf() IRefContext

	// GetTxt returns the txt rule contexts.
	GetTxt() ITextContext

	// GetFmttxt returns the fmttxt rule contexts.
	GetFmttxt() IFmtTextContext

	// GetFun returns the fun rule contexts.
	GetFun() IFunctionContext

	// GetCll returns the cll rule contexts.
	GetCll() ICallContext

	// GetMp returns the mp rule contexts.
	GetMp() IMappingContext

	// GetCmt returns the cmt rule contexts.
	GetCmt() ICommentContext

	// SetRf sets the rf rule contexts.
	SetRf(IRefContext)

	// SetTxt sets the txt rule contexts.
	SetTxt(ITextContext)

	// SetFmttxt sets the fmttxt rule contexts.
	SetFmttxt(IFmtTextContext)

	// SetFun sets the fun rule contexts.
	SetFun(IFunctionContext)

	// SetCll sets the cll rule contexts.
	SetCll(ICallContext)

	// SetMp sets the mp rule contexts.
	SetMp(IMappingContext)

	// SetCmt sets the cmt rule contexts.
	SetCmt(ICommentContext)

	// IsObjectDividerContext differentiates from other interfaces.
	IsObjectDividerContext()
}

type ObjectDividerContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	rf     IRefContext
	txt    ITextContext
	fmttxt IFmtTextContext
	fun    IFunctionContext
	cll    ICallContext
	mp     IMappingContext
	cmt    ICommentContext
}

func NewEmptyObjectDividerContext() *ObjectDividerContext {
	var p = new(ObjectDividerContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_objectDivider
	return p
}

func (*ObjectDividerContext) IsObjectDividerContext() {}

func NewObjectDividerContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ObjectDividerContext {
	var p = new(ObjectDividerContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_objectDivider

	return p
}

func (s *ObjectDividerContext) GetParser() antlr.Parser { return s.parser }

func (s *ObjectDividerContext) GetRf() IRefContext { return s.rf }

func (s *ObjectDividerContext) GetTxt() ITextContext { return s.txt }

func (s *ObjectDividerContext) GetFmttxt() IFmtTextContext { return s.fmttxt }

func (s *ObjectDividerContext) GetFun() IFunctionContext { return s.fun }

func (s *ObjectDividerContext) GetCll() ICallContext { return s.cll }

func (s *ObjectDividerContext) GetMp() IMappingContext { return s.mp }

func (s *ObjectDividerContext) GetCmt() ICommentContext { return s.cmt }

func (s *ObjectDividerContext) SetRf(v IRefContext) { s.rf = v }

func (s *ObjectDividerContext) SetTxt(v ITextContext) { s.txt = v }

func (s *ObjectDividerContext) SetFmttxt(v IFmtTextContext) { s.fmttxt = v }

func (s *ObjectDividerContext) SetFun(v IFunctionContext) { s.fun = v }

func (s *ObjectDividerContext) SetCll(v ICallContext) { s.cll = v }

func (s *ObjectDividerContext) SetMp(v IMappingContext) { s.mp = v }

func (s *ObjectDividerContext) SetCmt(v ICommentContext) { s.cmt = v }

func (s *ObjectDividerContext) SPACE() antlr.TerminalNode {
	return s.GetToken(CoaParserSPACE, 0)
}

func (s *ObjectDividerContext) Ref() IRefContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRefContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IRefContext)
}

func (s *ObjectDividerContext) Text() ITextContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITextContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITextContext)
}

func (s *ObjectDividerContext) FmtText() IFmtTextContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFmtTextContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFmtTextContext)
}

func (s *ObjectDividerContext) Function() IFunctionContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFunctionContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFunctionContext)
}

func (s *ObjectDividerContext) Call() ICallContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICallContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ICallContext)
}

func (s *ObjectDividerContext) Mapping() IMappingContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMappingContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMappingContext)
}

func (s *ObjectDividerContext) NEWLINE() antlr.TerminalNode {
	return s.GetToken(CoaParserNEWLINE, 0)
}

func (s *ObjectDividerContext) Comment() ICommentContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICommentContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ICommentContext)
}

func (s *ObjectDividerContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ObjectDividerContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ObjectDividerContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterObjectDivider(s)
	}
}

func (s *ObjectDividerContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitObjectDivider(s)
	}
}

func (p *CoaParser) ObjectDivider() (localctx IObjectDividerContext) {
	localctx = NewObjectDividerContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, CoaParserRULE_objectDivider)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(89)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 3, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(68)

			var _x = p.Ref()

			localctx.(*ObjectDividerContext).rf = _x
		}
		{
			p.SetState(69)
			p.Match(CoaParserSPACE)
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(71)

			var _x = p.Text()

			localctx.(*ObjectDividerContext).txt = _x
		}
		{
			p.SetState(72)
			p.Match(CoaParserSPACE)
		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(74)

			var _x = p.FmtText()

			localctx.(*ObjectDividerContext).fmttxt = _x
		}
		{
			p.SetState(75)
			p.Match(CoaParserSPACE)
		}

	case 4:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(77)

			var _x = p.Function()

			localctx.(*ObjectDividerContext).fun = _x
		}
		{
			p.SetState(78)
			p.Match(CoaParserSPACE)
		}

	case 5:
		p.EnterOuterAlt(localctx, 5)
		{
			p.SetState(80)

			var _x = p.Call()

			localctx.(*ObjectDividerContext).cll = _x
		}
		{
			p.SetState(81)
			p.Match(CoaParserSPACE)
		}

	case 6:
		p.EnterOuterAlt(localctx, 6)
		{
			p.SetState(83)

			var _x = p.Mapping()

			localctx.(*ObjectDividerContext).mp = _x
		}
		{
			p.SetState(84)
			p.Match(CoaParserSPACE)
		}

	case 7:
		p.EnterOuterAlt(localctx, 7)
		{
			p.SetState(86)

			var _x = p.Comment()

			localctx.(*ObjectDividerContext).cmt = _x
		}
		{
			p.SetState(87)
			p.Match(CoaParserNEWLINE)
		}

	}

	return localctx
}

// IRefContext is an interface to support dynamic dispatch.
type IRefContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetRfins returns the rfins rule contexts.
	GetRfins() IRefInsideContext

	// GetRfinsndot returns the rfinsndot rule contexts.
	GetRfinsndot() IRefInsideNoDotContext

	// SetRfins sets the rfins rule contexts.
	SetRfins(IRefInsideContext)

	// SetRfinsndot sets the rfinsndot rule contexts.
	SetRfinsndot(IRefInsideNoDotContext)

	// IsRefContext differentiates from other interfaces.
	IsRefContext()
}

type RefContext struct {
	*antlr.BaseParserRuleContext
	parser    antlr.Parser
	rfins     IRefInsideContext
	rfinsndot IRefInsideNoDotContext
}

func NewEmptyRefContext() *RefContext {
	var p = new(RefContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_ref
	return p
}

func (*RefContext) IsRefContext() {}

func NewRefContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RefContext {
	var p = new(RefContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_ref

	return p
}

func (s *RefContext) GetParser() antlr.Parser { return s.parser }

func (s *RefContext) GetRfins() IRefInsideContext { return s.rfins }

func (s *RefContext) GetRfinsndot() IRefInsideNoDotContext { return s.rfinsndot }

func (s *RefContext) SetRfins(v IRefInsideContext) { s.rfins = v }

func (s *RefContext) SetRfinsndot(v IRefInsideNoDotContext) { s.rfinsndot = v }

func (s *RefContext) RefInsideNoDot() IRefInsideNoDotContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRefInsideNoDotContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IRefInsideNoDotContext)
}

func (s *RefContext) AllRefInside() []IRefInsideContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IRefInsideContext)(nil)).Elem())
	var tst = make([]IRefInsideContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IRefInsideContext)
		}
	}

	return tst
}

func (s *RefContext) RefInside(i int) IRefInsideContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRefInsideContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IRefInsideContext)
}

func (s *RefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RefContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RefContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterRef(s)
	}
}

func (s *RefContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitRef(s)
	}
}

func (p *CoaParser) Ref() (localctx IRefContext) {
	localctx = NewRefContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, CoaParserRULE_ref)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(94)
	p.GetErrorHandler().Sync(p)
	_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 4, p.GetParserRuleContext())

	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			{
				p.SetState(91)

				var _x = p.RefInside()

				localctx.(*RefContext).rfins = _x
			}

		}
		p.SetState(96)
		p.GetErrorHandler().Sync(p)
		_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 4, p.GetParserRuleContext())
	}
	{
		p.SetState(97)

		var _x = p.RefInsideNoDot()

		localctx.(*RefContext).rfinsndot = _x
	}

	return localctx
}

// IRefInsideContext is an interface to support dynamic dispatch.
type IRefInsideContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetIdfmt returns the idfmt rule contexts.
	GetIdfmt() IIdFmtedContext

	// SetIdfmt sets the idfmt rule contexts.
	SetIdfmt(IIdFmtedContext)

	// IsRefInsideContext differentiates from other interfaces.
	IsRefInsideContext()
}

type RefInsideContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	idfmt  IIdFmtedContext
}

func NewEmptyRefInsideContext() *RefInsideContext {
	var p = new(RefInsideContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_refInside
	return p
}

func (*RefInsideContext) IsRefInsideContext() {}

func NewRefInsideContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RefInsideContext {
	var p = new(RefInsideContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_refInside

	return p
}

func (s *RefInsideContext) GetParser() antlr.Parser { return s.parser }

func (s *RefInsideContext) GetIdfmt() IIdFmtedContext { return s.idfmt }

func (s *RefInsideContext) SetIdfmt(v IIdFmtedContext) { s.idfmt = v }

func (s *RefInsideContext) DOT() antlr.TerminalNode {
	return s.GetToken(CoaParserDOT, 0)
}

func (s *RefInsideContext) IdFmted() IIdFmtedContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdFmtedContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdFmtedContext)
}

func (s *RefInsideContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RefInsideContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RefInsideContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterRefInside(s)
	}
}

func (s *RefInsideContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitRefInside(s)
	}
}

func (p *CoaParser) RefInside() (localctx IRefInsideContext) {
	localctx = NewRefInsideContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, CoaParserRULE_refInside)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(99)

		var _x = p.IdFmted()

		localctx.(*RefInsideContext).idfmt = _x
	}
	{
		p.SetState(100)
		p.Match(CoaParserDOT)
	}

	return localctx
}

// IRefInsideNoDotContext is an interface to support dynamic dispatch.
type IRefInsideNoDotContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetIdfmt returns the idfmt rule contexts.
	GetIdfmt() IIdFmtedContext

	// SetIdfmt sets the idfmt rule contexts.
	SetIdfmt(IIdFmtedContext)

	// IsRefInsideNoDotContext differentiates from other interfaces.
	IsRefInsideNoDotContext()
}

type RefInsideNoDotContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	idfmt  IIdFmtedContext
}

func NewEmptyRefInsideNoDotContext() *RefInsideNoDotContext {
	var p = new(RefInsideNoDotContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_refInsideNoDot
	return p
}

func (*RefInsideNoDotContext) IsRefInsideNoDotContext() {}

func NewRefInsideNoDotContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RefInsideNoDotContext {
	var p = new(RefInsideNoDotContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_refInsideNoDot

	return p
}

func (s *RefInsideNoDotContext) GetParser() antlr.Parser { return s.parser }

func (s *RefInsideNoDotContext) GetIdfmt() IIdFmtedContext { return s.idfmt }

func (s *RefInsideNoDotContext) SetIdfmt(v IIdFmtedContext) { s.idfmt = v }

func (s *RefInsideNoDotContext) IdFmted() IIdFmtedContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdFmtedContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdFmtedContext)
}

func (s *RefInsideNoDotContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RefInsideNoDotContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RefInsideNoDotContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterRefInsideNoDot(s)
	}
}

func (s *RefInsideNoDotContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitRefInsideNoDot(s)
	}
}

func (p *CoaParser) RefInsideNoDot() (localctx IRefInsideNoDotContext) {
	localctx = NewRefInsideNoDotContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, CoaParserRULE_refInsideNoDot)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(102)

		var _x = p.IdFmted()

		localctx.(*RefInsideNoDotContext).idfmt = _x
	}

	return localctx
}

// ITextContext is an interface to support dynamic dispatch.
type ITextContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetTxtnil returns the txtnil rule contexts.
	GetTxtnil() ITextNilContext

	// GetTxtnnil returns the txtnnil rule contexts.
	GetTxtnnil() ITextNonNilContext

	// SetTxtnil sets the txtnil rule contexts.
	SetTxtnil(ITextNilContext)

	// SetTxtnnil sets the txtnnil rule contexts.
	SetTxtnnil(ITextNonNilContext)

	// IsTextContext differentiates from other interfaces.
	IsTextContext()
}

type TextContext struct {
	*antlr.BaseParserRuleContext
	parser  antlr.Parser
	txtnil  ITextNilContext
	txtnnil ITextNonNilContext
}

func NewEmptyTextContext() *TextContext {
	var p = new(TextContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_text
	return p
}

func (*TextContext) IsTextContext() {}

func NewTextContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TextContext {
	var p = new(TextContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_text

	return p
}

func (s *TextContext) GetParser() antlr.Parser { return s.parser }

func (s *TextContext) GetTxtnil() ITextNilContext { return s.txtnil }

func (s *TextContext) GetTxtnnil() ITextNonNilContext { return s.txtnnil }

func (s *TextContext) SetTxtnil(v ITextNilContext) { s.txtnil = v }

func (s *TextContext) SetTxtnnil(v ITextNonNilContext) { s.txtnnil = v }

func (s *TextContext) TextNil() ITextNilContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITextNilContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITextNilContext)
}

func (s *TextContext) TextNonNil() ITextNonNilContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITextNonNilContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITextNonNilContext)
}

func (s *TextContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TextContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TextContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterText(s)
	}
}

func (s *TextContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitText(s)
	}
}

func (p *CoaParser) Text() (localctx ITextContext) {
	localctx = NewTextContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, CoaParserRULE_text)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(106)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 5, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(104)

			var _x = p.TextNil()

			localctx.(*TextContext).txtnil = _x
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(105)

			var _x = p.TextNonNil()

			localctx.(*TextContext).txtnnil = _x
		}

	}

	return localctx
}

// ITextNonNilContext is an interface to support dynamic dispatch.
type ITextNonNilContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetTxtfmt returns the txtfmt rule contexts.
	GetTxtfmt() ITextFmtedContext

	// SetTxtfmt sets the txtfmt rule contexts.
	SetTxtfmt(ITextFmtedContext)

	// IsTextNonNilContext differentiates from other interfaces.
	IsTextNonNilContext()
}

type TextNonNilContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	txtfmt ITextFmtedContext
}

func NewEmptyTextNonNilContext() *TextNonNilContext {
	var p = new(TextNonNilContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_textNonNil
	return p
}

func (*TextNonNilContext) IsTextNonNilContext() {}

func NewTextNonNilContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TextNonNilContext {
	var p = new(TextNonNilContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_textNonNil

	return p
}

func (s *TextNonNilContext) GetParser() antlr.Parser { return s.parser }

func (s *TextNonNilContext) GetTxtfmt() ITextFmtedContext { return s.txtfmt }

func (s *TextNonNilContext) SetTxtfmt(v ITextFmtedContext) { s.txtfmt = v }

func (s *TextNonNilContext) AllSINGLEQUOTE() []antlr.TerminalNode {
	return s.GetTokens(CoaParserSINGLEQUOTE)
}

func (s *TextNonNilContext) SINGLEQUOTE(i int) antlr.TerminalNode {
	return s.GetToken(CoaParserSINGLEQUOTE, i)
}

func (s *TextNonNilContext) TextFmted() ITextFmtedContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITextFmtedContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITextFmtedContext)
}

func (s *TextNonNilContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TextNonNilContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TextNonNilContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterTextNonNil(s)
	}
}

func (s *TextNonNilContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitTextNonNil(s)
	}
}

func (p *CoaParser) TextNonNil() (localctx ITextNonNilContext) {
	localctx = NewTextNonNilContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, CoaParserRULE_textNonNil)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(108)
		p.Match(CoaParserSINGLEQUOTE)
	}
	{
		p.SetState(109)

		var _x = p.TextFmted()

		localctx.(*TextNonNilContext).txtfmt = _x
	}
	{
		p.SetState(110)
		p.Match(CoaParserSINGLEQUOTE)
	}

	return localctx
}

// ITextNilContext is an interface to support dynamic dispatch.
type ITextNilContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsTextNilContext differentiates from other interfaces.
	IsTextNilContext()
}

type TextNilContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTextNilContext() *TextNilContext {
	var p = new(TextNilContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_textNil
	return p
}

func (*TextNilContext) IsTextNilContext() {}

func NewTextNilContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TextNilContext {
	var p = new(TextNilContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_textNil

	return p
}

func (s *TextNilContext) GetParser() antlr.Parser { return s.parser }

func (s *TextNilContext) AllSINGLEQUOTE() []antlr.TerminalNode {
	return s.GetTokens(CoaParserSINGLEQUOTE)
}

func (s *TextNilContext) SINGLEQUOTE(i int) antlr.TerminalNode {
	return s.GetToken(CoaParserSINGLEQUOTE, i)
}

func (s *TextNilContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TextNilContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TextNilContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterTextNil(s)
	}
}

func (s *TextNilContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitTextNil(s)
	}
}

func (p *CoaParser) TextNil() (localctx ITextNilContext) {
	localctx = NewTextNilContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 22, CoaParserRULE_textNil)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(112)
		p.Match(CoaParserSINGLEQUOTE)
	}
	{
		p.SetState(113)
		p.Match(CoaParserSINGLEQUOTE)
	}

	return localctx
}

// ITextFmtedContext is an interface to support dynamic dispatch.
type ITextFmtedContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetFt returns the ft rule contexts.
	GetFt() IFmtTextContext

	// SetFt sets the ft rule contexts.
	SetFt(IFmtTextContext)

	// IsTextFmtedContext differentiates from other interfaces.
	IsTextFmtedContext()
}

type TextFmtedContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	ft     IFmtTextContext
}

func NewEmptyTextFmtedContext() *TextFmtedContext {
	var p = new(TextFmtedContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_textFmted
	return p
}

func (*TextFmtedContext) IsTextFmtedContext() {}

func NewTextFmtedContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TextFmtedContext {
	var p = new(TextFmtedContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_textFmted

	return p
}

func (s *TextFmtedContext) GetParser() antlr.Parser { return s.parser }

func (s *TextFmtedContext) GetFt() IFmtTextContext { return s.ft }

func (s *TextFmtedContext) SetFt(v IFmtTextContext) { s.ft = v }

func (s *TextFmtedContext) AllIDINSIDE() []antlr.TerminalNode {
	return s.GetTokens(CoaParserIDINSIDE)
}

func (s *TextFmtedContext) IDINSIDE(i int) antlr.TerminalNode {
	return s.GetToken(CoaParserIDINSIDE, i)
}

func (s *TextFmtedContext) AllFmtText() []IFmtTextContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IFmtTextContext)(nil)).Elem())
	var tst = make([]IFmtTextContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IFmtTextContext)
		}
	}

	return tst
}

func (s *TextFmtedContext) FmtText(i int) IFmtTextContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFmtTextContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IFmtTextContext)
}

func (s *TextFmtedContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TextFmtedContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TextFmtedContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterTextFmted(s)
	}
}

func (s *TextFmtedContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitTextFmted(s)
	}
}

func (p *CoaParser) TextFmted() (localctx ITextFmtedContext) {
	localctx = NewTextFmtedContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 24, CoaParserRULE_textFmted)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(121)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == CoaParserIDINSIDE {
		{
			p.SetState(115)
			p.Match(CoaParserIDINSIDE)
		}
		p.SetState(117)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		if _la == CoaParserDOUBLEQUOTE {
			{
				p.SetState(116)

				var _x = p.FmtText()

				localctx.(*TextFmtedContext).ft = _x
			}

		}

		p.SetState(123)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IIdFmtedContext is an interface to support dynamic dispatch.
type IIdFmtedContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetFt returns the ft rule contexts.
	GetFt() IFmtTextContext

	// SetFt sets the ft rule contexts.
	SetFt(IFmtTextContext)

	// IsIdFmtedContext differentiates from other interfaces.
	IsIdFmtedContext()
}

type IdFmtedContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	ft     IFmtTextContext
}

func NewEmptyIdFmtedContext() *IdFmtedContext {
	var p = new(IdFmtedContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_idFmted
	return p
}

func (*IdFmtedContext) IsIdFmtedContext() {}

func NewIdFmtedContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IdFmtedContext {
	var p = new(IdFmtedContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_idFmted

	return p
}

func (s *IdFmtedContext) GetParser() antlr.Parser { return s.parser }

func (s *IdFmtedContext) GetFt() IFmtTextContext { return s.ft }

func (s *IdFmtedContext) SetFt(v IFmtTextContext) { s.ft = v }

func (s *IdFmtedContext) AllIDINSIDE() []antlr.TerminalNode {
	return s.GetTokens(CoaParserIDINSIDE)
}

func (s *IdFmtedContext) IDINSIDE(i int) antlr.TerminalNode {
	return s.GetToken(CoaParserIDINSIDE, i)
}

func (s *IdFmtedContext) AllFmtText() []IFmtTextContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IFmtTextContext)(nil)).Elem())
	var tst = make([]IFmtTextContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IFmtTextContext)
		}
	}

	return tst
}

func (s *IdFmtedContext) FmtText(i int) IFmtTextContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFmtTextContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IFmtTextContext)
}

func (s *IdFmtedContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdFmtedContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IdFmtedContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterIdFmted(s)
	}
}

func (s *IdFmtedContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitIdFmted(s)
	}
}

func (p *CoaParser) IdFmted() (localctx IIdFmtedContext) {
	localctx = NewIdFmtedContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 26, CoaParserRULE_idFmted)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(130)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == CoaParserIDINSIDE {
		{
			p.SetState(124)
			p.Match(CoaParserIDINSIDE)
		}
		p.SetState(126)
		p.GetErrorHandler().Sync(p)

		if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 8, p.GetParserRuleContext()) == 1 {
			{
				p.SetState(125)

				var _x = p.FmtText()

				localctx.(*IdFmtedContext).ft = _x
			}

		}

		p.SetState(132)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IFmtTextContext is an interface to support dynamic dispatch.
type IFmtTextContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetRb returns the rb rule contexts.
	GetRb() IRawBlockContext

	// SetRb sets the rb rule contexts.
	SetRb(IRawBlockContext)

	// IsFmtTextContext differentiates from other interfaces.
	IsFmtTextContext()
}

type FmtTextContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	rb     IRawBlockContext
}

func NewEmptyFmtTextContext() *FmtTextContext {
	var p = new(FmtTextContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_fmtText
	return p
}

func (*FmtTextContext) IsFmtTextContext() {}

func NewFmtTextContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FmtTextContext {
	var p = new(FmtTextContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_fmtText

	return p
}

func (s *FmtTextContext) GetParser() antlr.Parser { return s.parser }

func (s *FmtTextContext) GetRb() IRawBlockContext { return s.rb }

func (s *FmtTextContext) SetRb(v IRawBlockContext) { s.rb = v }

func (s *FmtTextContext) AllDOUBLEQUOTE() []antlr.TerminalNode {
	return s.GetTokens(CoaParserDOUBLEQUOTE)
}

func (s *FmtTextContext) DOUBLEQUOTE(i int) antlr.TerminalNode {
	return s.GetToken(CoaParserDOUBLEQUOTE, i)
}

func (s *FmtTextContext) RawBlock() IRawBlockContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRawBlockContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IRawBlockContext)
}

func (s *FmtTextContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FmtTextContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FmtTextContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterFmtText(s)
	}
}

func (s *FmtTextContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitFmtText(s)
	}
}

func (p *CoaParser) FmtText() (localctx IFmtTextContext) {
	localctx = NewFmtTextContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 28, CoaParserRULE_fmtText)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(133)
		p.Match(CoaParserDOUBLEQUOTE)
	}
	{
		p.SetState(134)

		var _x = p.RawBlock()

		localctx.(*FmtTextContext).rb = _x
	}
	{
		p.SetState(135)
		p.Match(CoaParserDOUBLEQUOTE)
	}

	return localctx
}

// IFunctionContext is an interface to support dynamic dispatch.
type IFunctionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetMp returns the mp rule contexts.
	GetMp() IMappingContext

	// GetBlk returns the blk rule contexts.
	GetBlk() IBlockContext

	// SetMp sets the mp rule contexts.
	SetMp(IMappingContext)

	// SetBlk sets the blk rule contexts.
	SetBlk(IBlockContext)

	// IsFunctionContext differentiates from other interfaces.
	IsFunctionContext()
}

type FunctionContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	mp     IMappingContext
	blk    IBlockContext
}

func NewEmptyFunctionContext() *FunctionContext {
	var p = new(FunctionContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_function
	return p
}

func (*FunctionContext) IsFunctionContext() {}

func NewFunctionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FunctionContext {
	var p = new(FunctionContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_function

	return p
}

func (s *FunctionContext) GetParser() antlr.Parser { return s.parser }

func (s *FunctionContext) GetMp() IMappingContext { return s.mp }

func (s *FunctionContext) GetBlk() IBlockContext { return s.blk }

func (s *FunctionContext) SetMp(v IMappingContext) { s.mp = v }

func (s *FunctionContext) SetBlk(v IBlockContext) { s.blk = v }

func (s *FunctionContext) Mapping() IMappingContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMappingContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMappingContext)
}

func (s *FunctionContext) Block() IBlockContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBlockContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBlockContext)
}

func (s *FunctionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FunctionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FunctionContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterFunction(s)
	}
}

func (s *FunctionContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitFunction(s)
	}
}

func (p *CoaParser) Function() (localctx IFunctionContext) {
	localctx = NewFunctionContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 30, CoaParserRULE_function)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(137)

		var _x = p.Mapping()

		localctx.(*FunctionContext).mp = _x
	}
	{
		p.SetState(138)

		var _x = p.Block()

		localctx.(*FunctionContext).blk = _x
	}

	return localctx
}

// ICallContext is an interface to support dynamic dispatch.
type ICallContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetRf returns the rf rule contexts.
	GetRf() IRefContext

	// GetMp returns the mp rule contexts.
	GetMp() IMappingContext

	// SetRf sets the rf rule contexts.
	SetRf(IRefContext)

	// SetMp sets the mp rule contexts.
	SetMp(IMappingContext)

	// IsCallContext differentiates from other interfaces.
	IsCallContext()
}

type CallContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	rf     IRefContext
	mp     IMappingContext
}

func NewEmptyCallContext() *CallContext {
	var p = new(CallContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_call
	return p
}

func (*CallContext) IsCallContext() {}

func NewCallContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CallContext {
	var p = new(CallContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_call

	return p
}

func (s *CallContext) GetParser() antlr.Parser { return s.parser }

func (s *CallContext) GetRf() IRefContext { return s.rf }

func (s *CallContext) GetMp() IMappingContext { return s.mp }

func (s *CallContext) SetRf(v IRefContext) { s.rf = v }

func (s *CallContext) SetMp(v IMappingContext) { s.mp = v }

func (s *CallContext) Ref() IRefContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRefContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IRefContext)
}

func (s *CallContext) Mapping() IMappingContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMappingContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMappingContext)
}

func (s *CallContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CallContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CallContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterCall(s)
	}
}

func (s *CallContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitCall(s)
	}
}

func (p *CoaParser) Call() (localctx ICallContext) {
	localctx = NewCallContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 32, CoaParserRULE_call)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(140)

		var _x = p.Ref()

		localctx.(*CallContext).rf = _x
	}
	{
		p.SetState(141)

		var _x = p.Mapping()

		localctx.(*CallContext).mp = _x
	}

	return localctx
}

// IMappingContext is an interface to support dynamic dispatch.
type IMappingContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetMpnnil returns the mpnnil rule contexts.
	GetMpnnil() IMapNonNilContext

	// SetMpnnil sets the mpnnil rule contexts.
	SetMpnnil(IMapNonNilContext)

	// IsMappingContext differentiates from other interfaces.
	IsMappingContext()
}

type MappingContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	mpnnil IMapNonNilContext
}

func NewEmptyMappingContext() *MappingContext {
	var p = new(MappingContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_mapping
	return p
}

func (*MappingContext) IsMappingContext() {}

func NewMappingContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MappingContext {
	var p = new(MappingContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_mapping

	return p
}

func (s *MappingContext) GetParser() antlr.Parser { return s.parser }

func (s *MappingContext) GetMpnnil() IMapNonNilContext { return s.mpnnil }

func (s *MappingContext) SetMpnnil(v IMapNonNilContext) { s.mpnnil = v }

func (s *MappingContext) OPAREN() antlr.TerminalNode {
	return s.GetToken(CoaParserOPAREN, 0)
}

func (s *MappingContext) CPAREN() antlr.TerminalNode {
	return s.GetToken(CoaParserCPAREN, 0)
}

func (s *MappingContext) MapNonNil() IMapNonNilContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMapNonNilContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMapNonNilContext)
}

func (s *MappingContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MappingContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MappingContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterMapping(s)
	}
}

func (s *MappingContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitMapping(s)
	}
}

func (p *CoaParser) Mapping() (localctx IMappingContext) {
	localctx = NewMappingContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 34, CoaParserRULE_mapping)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(143)
		p.Match(CoaParserOPAREN)
	}
	p.SetState(145)
	p.GetErrorHandler().Sync(p)

	if p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 10, p.GetParserRuleContext()) == 1 {
		{
			p.SetState(144)

			var _x = p.MapNonNil()

			localctx.(*MappingContext).mpnnil = _x
		}

	}
	{
		p.SetState(147)
		p.Match(CoaParserCPAREN)
	}

	return localctx
}

// IMapNonNilContext is an interface to support dynamic dispatch.
type IMapNonNilContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetObjspc returns the objspc rule contexts.
	GetObjspc() IObjectSpaceContext

	// GetObj returns the obj rule contexts.
	GetObj() IObjectContext

	// SetObjspc sets the objspc rule contexts.
	SetObjspc(IObjectSpaceContext)

	// SetObj sets the obj rule contexts.
	SetObj(IObjectContext)

	// IsMapNonNilContext differentiates from other interfaces.
	IsMapNonNilContext()
}

type MapNonNilContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	objspc IObjectSpaceContext
	obj    IObjectContext
}

func NewEmptyMapNonNilContext() *MapNonNilContext {
	var p = new(MapNonNilContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_mapNonNil
	return p
}

func (*MapNonNilContext) IsMapNonNilContext() {}

func NewMapNonNilContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MapNonNilContext {
	var p = new(MapNonNilContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_mapNonNil

	return p
}

func (s *MapNonNilContext) GetParser() antlr.Parser { return s.parser }

func (s *MapNonNilContext) GetObjspc() IObjectSpaceContext { return s.objspc }

func (s *MapNonNilContext) GetObj() IObjectContext { return s.obj }

func (s *MapNonNilContext) SetObjspc(v IObjectSpaceContext) { s.objspc = v }

func (s *MapNonNilContext) SetObj(v IObjectContext) { s.obj = v }

func (s *MapNonNilContext) Object() IObjectContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IObjectContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IObjectContext)
}

func (s *MapNonNilContext) AllObjectSpace() []IObjectSpaceContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IObjectSpaceContext)(nil)).Elem())
	var tst = make([]IObjectSpaceContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IObjectSpaceContext)
		}
	}

	return tst
}

func (s *MapNonNilContext) ObjectSpace(i int) IObjectSpaceContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IObjectSpaceContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IObjectSpaceContext)
}

func (s *MapNonNilContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MapNonNilContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MapNonNilContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterMapNonNil(s)
	}
}

func (s *MapNonNilContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitMapNonNil(s)
	}
}

func (p *CoaParser) MapNonNil() (localctx IMapNonNilContext) {
	localctx = NewMapNonNilContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 36, CoaParserRULE_mapNonNil)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(152)
	p.GetErrorHandler().Sync(p)
	_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 11, p.GetParserRuleContext())

	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			{
				p.SetState(149)

				var _x = p.ObjectSpace()

				localctx.(*MapNonNilContext).objspc = _x
			}

		}
		p.SetState(154)
		p.GetErrorHandler().Sync(p)
		_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 11, p.GetParserRuleContext())
	}
	{
		p.SetState(155)

		var _x = p.Object()

		localctx.(*MapNonNilContext).obj = _x
	}

	return localctx
}

// IObjectSpaceContext is an interface to support dynamic dispatch.
type IObjectSpaceContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetMpins returns the mpins rule contexts.
	GetMpins() IObjectContext

	// SetMpins sets the mpins rule contexts.
	SetMpins(IObjectContext)

	// IsObjectSpaceContext differentiates from other interfaces.
	IsObjectSpaceContext()
}

type ObjectSpaceContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	mpins  IObjectContext
}

func NewEmptyObjectSpaceContext() *ObjectSpaceContext {
	var p = new(ObjectSpaceContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_objectSpace
	return p
}

func (*ObjectSpaceContext) IsObjectSpaceContext() {}

func NewObjectSpaceContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ObjectSpaceContext {
	var p = new(ObjectSpaceContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_objectSpace

	return p
}

func (s *ObjectSpaceContext) GetParser() antlr.Parser { return s.parser }

func (s *ObjectSpaceContext) GetMpins() IObjectContext { return s.mpins }

func (s *ObjectSpaceContext) SetMpins(v IObjectContext) { s.mpins = v }

func (s *ObjectSpaceContext) SPACE() antlr.TerminalNode {
	return s.GetToken(CoaParserSPACE, 0)
}

func (s *ObjectSpaceContext) Object() IObjectContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IObjectContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IObjectContext)
}

func (s *ObjectSpaceContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ObjectSpaceContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ObjectSpaceContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterObjectSpace(s)
	}
}

func (s *ObjectSpaceContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitObjectSpace(s)
	}
}

func (p *CoaParser) ObjectSpace() (localctx IObjectSpaceContext) {
	localctx = NewObjectSpaceContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 38, CoaParserRULE_objectSpace)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(157)

		var _x = p.Object()

		localctx.(*ObjectSpaceContext).mpins = _x
	}
	{
		p.SetState(158)
		p.Match(CoaParserSPACE)
	}

	return localctx
}

// ICommentContext is an interface to support dynamic dispatch.
type ICommentContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetCmtins returns the cmtins token.
	GetCmtins() antlr.Token

	// SetCmtins sets the cmtins token.
	SetCmtins(antlr.Token)

	// IsCommentContext differentiates from other interfaces.
	IsCommentContext()
}

type CommentContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	cmtins antlr.Token
}

func NewEmptyCommentContext() *CommentContext {
	var p = new(CommentContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = CoaParserRULE_comment
	return p
}

func (*CommentContext) IsCommentContext() {}

func NewCommentContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CommentContext {
	var p = new(CommentContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = CoaParserRULE_comment

	return p
}

func (s *CommentContext) GetParser() antlr.Parser { return s.parser }

func (s *CommentContext) GetCmtins() antlr.Token { return s.cmtins }

func (s *CommentContext) SetCmtins(v antlr.Token) { s.cmtins = v }

func (s *CommentContext) NUMBERSIGN() antlr.TerminalNode {
	return s.GetToken(CoaParserNUMBERSIGN, 0)
}

func (s *CommentContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CommentContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CommentContext) EnterRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.EnterComment(s)
	}
}

func (s *CommentContext) ExitRule(listener antlr.ParseTreeListener) {
	if listenerT, ok := listener.(CoaListener); ok {
		listenerT.ExitComment(s)
	}
}

func (p *CoaParser) Comment() (localctx ICommentContext) {
	localctx = NewCommentContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 40, CoaParserRULE_comment)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	var _alt int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(160)
		p.Match(CoaParserNUMBERSIGN)
	}
	p.SetState(164)
	p.GetErrorHandler().Sync(p)
	_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 12, p.GetParserRuleContext())

	for _alt != 1 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1+1 {
			p.SetState(161)

			var _mwc = p.MatchWildcard()

			localctx.(*CommentContext).cmtins = _mwc

		}
		p.SetState(166)
		p.GetErrorHandler().Sync(p)
		_alt = p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 12, p.GetParserRuleContext())
	}

	return localctx
}
