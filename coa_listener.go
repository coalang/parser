// Code generated from Coa.g4 by ANTLR 4.8. DO NOT EDIT.

package parser // Coa

import "github.com/antlr/antlr4/runtime/Go/antlr"

// CoaListener is a complete listener for a parse tree produced by CoaParser.
type CoaListener interface {
	antlr.ParseTreeListener

	// EnterStart is called when entering the start production.
	EnterStart(c *StartContext)

	// EnterBlock is called when entering the block production.
	EnterBlock(c *BlockContext)

	// EnterRawBlock is called when entering the rawBlock production.
	EnterRawBlock(c *RawBlockContext)

	// EnterRawBlockNonNil is called when entering the rawBlockNonNil production.
	EnterRawBlockNonNil(c *RawBlockNonNilContext)

	// EnterObject is called when entering the object production.
	EnterObject(c *ObjectContext)

	// EnterObjectDivider is called when entering the objectDivider production.
	EnterObjectDivider(c *ObjectDividerContext)

	// EnterRef is called when entering the ref production.
	EnterRef(c *RefContext)

	// EnterRefInside is called when entering the refInside production.
	EnterRefInside(c *RefInsideContext)

	// EnterRefInsideNoDot is called when entering the refInsideNoDot production.
	EnterRefInsideNoDot(c *RefInsideNoDotContext)

	// EnterText is called when entering the text production.
	EnterText(c *TextContext)

	// EnterTextNonNil is called when entering the textNonNil production.
	EnterTextNonNil(c *TextNonNilContext)

	// EnterTextNil is called when entering the textNil production.
	EnterTextNil(c *TextNilContext)

	// EnterTextFmted is called when entering the textFmted production.
	EnterTextFmted(c *TextFmtedContext)

	// EnterIdFmted is called when entering the idFmted production.
	EnterIdFmted(c *IdFmtedContext)

	// EnterFmtText is called when entering the fmtText production.
	EnterFmtText(c *FmtTextContext)

	// EnterFunction is called when entering the function production.
	EnterFunction(c *FunctionContext)

	// EnterCall is called when entering the call production.
	EnterCall(c *CallContext)

	// EnterMapping is called when entering the mapping production.
	EnterMapping(c *MappingContext)

	// EnterMapNonNil is called when entering the mapNonNil production.
	EnterMapNonNil(c *MapNonNilContext)

	// EnterObjectSpace is called when entering the objectSpace production.
	EnterObjectSpace(c *ObjectSpaceContext)

	// EnterComment is called when entering the comment production.
	EnterComment(c *CommentContext)

	// ExitStart is called when exiting the start production.
	ExitStart(c *StartContext)

	// ExitBlock is called when exiting the block production.
	ExitBlock(c *BlockContext)

	// ExitRawBlock is called when exiting the rawBlock production.
	ExitRawBlock(c *RawBlockContext)

	// ExitRawBlockNonNil is called when exiting the rawBlockNonNil production.
	ExitRawBlockNonNil(c *RawBlockNonNilContext)

	// ExitObject is called when exiting the object production.
	ExitObject(c *ObjectContext)

	// ExitObjectDivider is called when exiting the objectDivider production.
	ExitObjectDivider(c *ObjectDividerContext)

	// ExitRef is called when exiting the ref production.
	ExitRef(c *RefContext)

	// ExitRefInside is called when exiting the refInside production.
	ExitRefInside(c *RefInsideContext)

	// ExitRefInsideNoDot is called when exiting the refInsideNoDot production.
	ExitRefInsideNoDot(c *RefInsideNoDotContext)

	// ExitText is called when exiting the text production.
	ExitText(c *TextContext)

	// ExitTextNonNil is called when exiting the textNonNil production.
	ExitTextNonNil(c *TextNonNilContext)

	// ExitTextNil is called when exiting the textNil production.
	ExitTextNil(c *TextNilContext)

	// ExitTextFmted is called when exiting the textFmted production.
	ExitTextFmted(c *TextFmtedContext)

	// ExitIdFmted is called when exiting the idFmted production.
	ExitIdFmted(c *IdFmtedContext)

	// ExitFmtText is called when exiting the fmtText production.
	ExitFmtText(c *FmtTextContext)

	// ExitFunction is called when exiting the function production.
	ExitFunction(c *FunctionContext)

	// ExitCall is called when exiting the call production.
	ExitCall(c *CallContext)

	// ExitMapping is called when exiting the mapping production.
	ExitMapping(c *MappingContext)

	// ExitMapNonNil is called when exiting the mapNonNil production.
	ExitMapNonNil(c *MapNonNilContext)

	// ExitObjectSpace is called when exiting the objectSpace production.
	ExitObjectSpace(c *ObjectSpaceContext)

	// ExitComment is called when exiting the comment production.
	ExitComment(c *CommentContext)
}
